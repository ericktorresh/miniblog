<?php
defined('SYSPATH') or die('No direct script access.');

class Controller_Home extends Controller
{
    public function action_index()
    {
        $data_entries = Request::factory('entry/entries')->execute();
        $view = Twig::factory('Home/default');
        $view->entries = $data_entries->body();
        $this->response->body($view);
    }
}
