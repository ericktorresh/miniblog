<?php
defined('SYSPATH') or die('No direct script access.');

/**
 * Description of Entry
 *
 * @author erickjose
 */
class Controller_Entry extends Controller
{
    /**
     * Create new entry object.
     * 
     * @return $response
     */
    public function action_create()
    {
        $response = [
            "success" => false,
            "data" => '',
            "message" => ""
        ];

        if (!Auth::instance()->logged_in())
        {
            if ($this->request->is_ajax())
            {
                $response['message'] = 'Forbidden, please log in.';
                $this->toJson($response);
            }
            Session::instance()->set("error", 'Forbidden, please log in.');
            $this->redirect("/");
        }

        $entry = null;

        $return_url = ($this->request->post("return")) ? $this->request->post("return") : "/";

        if (HTTP_Request::POST == $this->request->method())
        {

            $entry_body = $this->request->post("entry_body");

            if (trim($entry_body))
            {

                $user_logged = Auth::instance()->get_user();
                $mefdb = Mefdb::instance();
                $driver = $mefdb->get_driver();

                //Insert new entry
                $time = time();
                $st = $driver->prepare('INSERT INTO entry (body,created_at, created_by) VALUES(:body,:created_at, :created_by) ');
                $st->bindParameter(':body', $entry_body);
                $st->bindParameter(':created_at', $time);
                $st->bindParameter(':created_by', $user_logged['id']);
                $affectedRows = $st->execute();

                if ($affectedRows == 1)
                {
                    //Get last entry to return
                    $last_id = $mefdb->get_last_id();
                    $st = $driver->prepare('SELECT * FROM entry WHERE id = :id ');
                    $st->bindParameter(':id', $last_id);
                    $entry = $st->query()->fetchRow();

                    //Build response
                    //ToDo implement translation for messages
                    $response['message'] = 'Entry created succesfull';
                    $response["success"] = true;
                    $response['data'] = Twig::factory('Entry/Entry', [ 'entry' => $entry])->render();
                } else
                {
                    $response['message'] = 'Error to create entry. please, try again.';
                }
            } else
            {
                //ToDo implement translation for messages
                $response['message'] = 'Body can not be empty, please set one and try again.';
            }
        }

        if ($this->request->is_ajax())
        {
            $this->toJson($response);
        } else
        {
            Session::instance()->set(($response["success"]) ? "success" : "error", $response['message']);
            $this->redirect($return_url);
        }
    }

    /**
     * Get all entries
     */
    public function action_entries()
    {
        $driver = Mefdb::instance()->get_driver();
        
        //Callback function
        $assoc_user = function($row)
        {
            $row["fullname"] = $row["firstname"] . " " . $row["lastname"];
            return $row;
        };

        $entries = $driver->query('SELECT e.*,u.id uid, u.username, u.firstname, u.lastname, u.email 
                                    FROM entry e 
                                    LEFT JOIN user u ON u.id = e.created_by 
                                    ORDER BY e.id DESC')->fetchAllWithCallback($assoc_user);

        $view = Twig::factory('Entry/List', [ 'entries' => $entries]);
        $this->response->body($view);
    }

    /**
     * Get entries by user
     * @param id user
     */
    public function action_entries_by_user()
    {
        $uid = $this->request->param('id');
        $driver = Mefdb::instance()->get_driver();

        //That is great!! :)
        $assoc_user = function($row)
        {
            $row["fullname"] = $row["firstname"] . " " . $row["lastname"];
            return $row;
        };

        $st = $driver->prepare('SELECT e.*,u.id uid, u.username, u.firstname, u.lastname, u.email 
                                FROM entry e 
                                LEFT JOIN user u ON u.id = e.created_by 
                                WHERE u.id = :uid
                                ORDER BY e.id DESC');

        $st->bindParameter(':uid', $uid);
        $entries = $st->query()->fetchAllWithCallback($assoc_user);

        $view = Twig::factory('Entry/List', [ 'entries' => $entries]);
        $this->response->body($view);
    }

    /**
     * Function response in Json format
     */
    protected function toJson($response)
    {
        $this->response->headers('Content-Type', 'application/json');
        $this->response->body(json_encode($response));
    }
}