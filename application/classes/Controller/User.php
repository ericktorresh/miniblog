<?php
defined('SYSPATH') or die('No direct script access.');

/**
 * Description of User
 *
 * @author erickjose
 */
class Controller_User extends Controller
{

    /**
     * Display user profile
     */
    public function action_profile()
    {
        $username = $this->request->param('id');
        $driver = Mefdb::instance()->get_driver();
        $st = $driver->prepare('SELECT * FROM user WHERE username = :username');
        $st->bindParameter(':username', $username);
        $user = $st->query()->fetchRow();

        //If user doesn't exist, redirect
        if (!isset($user["id"]))
        {
            Session::instance()->set("error", 'User not found!');
            $this->redirect("/");
        }

        $data_entries = Request::factory('entry/entries_by_user/' . $user["id"])->execute();
        $view = Twig::factory('User/profile');
        $view->user = $user;
        $view->entries = $data_entries->body();
        $this->response->body($view);
    }

    /**
     * Show gallery by user
     * @return response json format
     */
    public function action_gallery()
    {
        $response = [
            "success" => false,
            "data" => '',
            "message" => ""
        ];

        $username = $this->request->param('id');
        $driver = Mefdb::instance()->get_driver();
        $st = $driver->prepare('SELECT * FROM user WHERE username = :username');
        $st->bindParameter(':username', $username);
        $user = $st->query()->fetchRow();

        //If user doesn't exist, redirect
        if (!isset($user["id"]))
        {
            $response['message'] = 'User not found!';
            $this->response->body($this->toJson($response));
        }

        $base_path = DOCROOT . 'media' . DIRECTORY_SEPARATOR . 'user' . DIRECTORY_SEPARATOR . $user["id"] . DIRECTORY_SEPARATOR . "gallery";

        if (is_dir($base_path))
        {
            $files_scanned = glob($base_path . '/*.{jpeg,jpg,gif,png}', GLOB_BRACE);
            $images = [];

            foreach ($files_scanned as $file)
            {
                $name = str_replace($base_path . '/', '', $file);
                $images[] = [
                    'title' => $name,
                    'source' => URL::base() . 'media/user/' . $user["id"] . '/gallery/' . $name
                ];
            }

            $response['success'] = true;
            $response['data'] = $images;
            $this->response->body($this->toJson($response));
        } else
        {
            $response['message'] = 'User gallery not found!';
            $this->response->body($this->toJson($response));
        }
    }

    /**
     * Display login form
     */
    public function action_login()
    {

        if (Auth::instance()->logged_in())
        {
            //Redirect to home
            $this->redirect("/");
        }

        $twig = Twig::factory('User/login');
        $this->response->body($twig);
    }

    /**
     * Function to check user data and do login
     */
    public function action_dologin()
    {

        if (Auth::instance()->logged_in())
        {
            //Redirect to home
            $this->redirect(URL::site("/"));
        }

        if (HTTP_Request::POST == $this->request->method())
        {

            $Auth = Auth::instance();
            $user = $Auth->login($this->request->post('username'), $this->request->post('password'));

            // If successful, redirect user
            if ($user)
            {
                $this->redirect('/');
            } else
            {
                Session::instance()->set("error", 'Username or password incorrect!');
            }
        }

        $twig = Twig::factory('User/login');
        $twig->username = $this->request->post('username');
        $this->response->body($twig);
    }

    /**
     * Logout action
     */
    public function action_logout()
    {

        if (Auth::instance()->logged_in())
        {
            //Redirect to home
            Auth::instance()->logout();
        }
        $this->redirect('/');
    }

    /**
     * Function response in Json format
     */
    protected function toJson($response)
    {
        $this->response->headers('Content-Type', 'application/json');
        $this->response->body(json_encode($response));
    }
}