<?php
defined('SYSPATH') or die('No direct script access.');

/**
 * Description of DB
 *
 * @author erick.jobsity
 */
class Auth_DB extends Auth
{
    private $driver;
    
    public function __construct($config = array())
    {
        parent::__construct($config);
        $this->driver = Mefdb::instance()->get_driver();
    }

    protected function _login($username, $password, $remember)
    {
        if (is_string($password))
        {
            // Create a hashed password
            $password = $this->hash($password);
        }

        $st = $this->driver->prepare('SELECT * FROM user WHERE username = :username');
        $st->setParameter(':username', $username);
        $user = $st->query()->fetchRow();

        if (isset($user['id']) AND ( $user['username'] == $username AND $user['password'] === $password))
        {
            // Complete the login
            unset($user["password"]);
            return (parent::complete_login($user)) ? $user : false;
        }

        // Login failed
        return false;
    }

    public function password($username)
    {
        $st = $this->driver->prepare('SELECT * FROM user WHERE username = :username');
        $st->setParameter(':username', $username);
        $user = $st->query()->fetchValue();

        if (isset($user['id']))
        {
            return $user['password'];
        }

        return false;
    }

    public function check_password($password)
    {
        $user_logged = $this->get_user();

        if ($user_logged === false)
        {
            return false;
        }

        return ($password === $this->password($user_logged["username"]));
    }

    public function logged_in($role = null)
    {
        return parent::logged_in($role);
    }

    public function get_user($default = null)
    {
        return parent::get_user($default);
    }

}