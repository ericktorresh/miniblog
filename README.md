# Mini Blog

Show timelines by user registred.

Functionalities: 

- Login
- General timeline
- User profile (timeline)

This application was built with Kohana framework, Twig template and MefDB.

##Instalation

1. Run composer install for dependencies
2. Run bower install
3. Restore backup SQL file named "/etc/miniblog.sql"
4. Copy and rename the file /application/config/default.database.php to /application/config/database.php
5. Setting up the database connection parameters.
6. Add the creation of the cache and logs folders in /application

##For testing
Run internal web server: 

    php bin/start_server.php 

The project uses 8000 port

Testing user (useraname/password)
    1. erickt/1234
    2. joset/1234