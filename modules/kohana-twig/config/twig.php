<?php defined('SYSPATH') or die('No direct script access.');

return array(

	/**
	 * Twig Loader options
	 */
	'loader' => array(
		'extension' => 'twig',  // Extension for Twig files
		'path'      => 'views', // Path within cascading filesystem for Twig files
	),

	/**
	 * Twig Environment options
	 *
	 * http://twig.sensiolabs.org/doc/api.html#environment-options
	 */
	'environment' => array(
		'auto_reload'         => (Kohana::$environment == Kohana::DEVELOPMENT),
		'autoescape'          => TRUE,
		'base_template_class' => 'Twig_Template',
		'cache'               => APPPATH.'cache/twig',
		'charset'             => 'utf-8',
		'optimizations'       => -1,
		'strict_variables'    => FALSE,
                'debug'               => true,
	),

	/**
	 * Custom functions, filters and tests
	 *
	 *     'functions' => array(
	 *         'my_method' => array('MyClass', 'my_method'),
	 *     ),
	 */
	'functions' => array(
            "URL"=>function($string_url){
                return URL::site($string_url);
            }
        ),
        'globals'=>[
            'Auth'=> Auth::instance(),
            'Session'=> Session::instance(),
            'Kohana'=> function(){
                return Kohana;
            }
        ],
	'filters' => array(),
        'tests' => array(),
        'helpers'=>[
            'HTML','Upload','Form','File','Arr','Text'
        ]

);
