<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Description of Mefdb
 *
 * @author erick.jobsity
 */
class Kohana_Mefdb
{

    protected static $_instance;
    public $type;

    public static function instance()
    {
        if (!Mefdb::$_instance instanceof Mefdb)
        {
            // Create a new instance
            Mefdb::$_instance = new Mefdb();
        }

        return Mefdb::$_instance;
    }

    protected $driver;

    public function __construct()
    {
        $this->connect();
    }

    protected function connect()
    {
        //Get configuration of database
        $config = Kohana::$config->load('database')->default;
        if (!isset($config['type']))
        {
            throw new Kohana_Exception('Database type not defined in :name configuration', array(':name' => $name));
        }
        $type = strtolower($config['type']);

        //Load appropiede driver of MedDB using driver selected in config Kohana database
        if ($type == 'pdo')
        {

            extract($config['connection'] + array(
                'dsn' => '',
                'username' => NULL,
                'password' => NULL,
                'persistent' => FALSE,
            ));
            $this->driver = new mef\Db\Driver\PdoDriver(new PDO($dsn, $username, $password));
        } elseif ($type == 'mysql')
        {

            extract($this->_config['connection'] + array(
                'database' => '',
                'hostname' => '',
                'username' => '',
                'password' => '',
                'persistent' => FALSE,
            ));

            $this->driver = new mef\Db\Driver\MysqliDriver(new mysqli($hostname, $username, $password));
        } else
        {
            throw new Kohana_Exception('Database type ' . $type . ' not supported by MefDb');
        }

        $this->type = $type;
    }

    public function get_last_id()
    {
        return $this->driver->query('SELECT LAST_INSERT_ID()')->fetchValue();
    }

    public function get_driver()
    {
        if (!$this->driver instanceof mef\Db\Driver\DriverInterface)
        {
            $this->connect();
        }
        return $this->driver;
    }

}
