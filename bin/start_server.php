<?php
extract(getopt('', ['host:', 'port:']) + ['host' => '0.0.0.0', 'port' => 8000]);
echo "Running server on $host:$port...\n";
shell_exec('php -S ' . escapeshellarg($host . ':' . $port) . ' -t ' . __DIR__ . '/../');
